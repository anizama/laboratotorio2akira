package com.example.avancelabo3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_destino.*
import kotlinx.android.synthetic.main.activity_main.*

class DestinoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destino)

        //1. Declaramos un BundleRecepcion
        val bundleRecepcion :Bundle? = intent.extras //si llega un null no se cae la aplicacion al usuario

       // if (bundleRecepcion != null){
            // 2. recepcionar put getString/Double/Senanizable(clase)
          //  val nombre = bundleRecepcion?.getString("KEY NOMBRES","") //usamos ? , luego ""
                // (sino existe devuelveme vacio: se da en la situacion  )
           // val edad = bundleRecepcion?.getString("KEY EDAD","")
            //val tipo = bundleRecepcion?.getString("KEY TIPO","")

        //LET
        bundleRecepcion?.let {LibredeNull -> //si no es null entra la sentencia, sino se queda afuera
             val nombre = bundleRecepcion?.getString("KEY NOMBRES","No ha ingresado su nombre")
             val edad = bundleRecepcion?.getString("KEY EDAD","No ha ingresado su edad")
             val tipo = bundleRecepcion?.getString("KEY TIPO","No ha seleccionado el tipo de mascota")

            tvnombreresultaado.text=nombre
            tvedadresultado.text=edad
            tvtiporesultado.text=tipo

            if (tipo =="PERRO"){
                tvtiporesultado.text="PERRO"
                img2.setImageResource(R.drawable.dog)
            }
            else if (tipo=="GATO") {
                tvtiporesultado.text="GATO"
                img2.setImageResource(R.drawable.cat)
            }
            else if (tipo=="CONEJO") {
                tvtiporesultado.text="CONEJO"
                img2.setImageResource(R.drawable.rabbit)
            }
        }


        // 3. Ahora lo anidamos a nuestros textview para que muestre los datos
           // tvnombreresultaado.text=nombre
            //tvedadresultado.text=edad
            //tvtiporesultado.text=tipo

        }


    }
